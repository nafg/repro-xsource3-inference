object Error {
  class OptGroup[A]
  type Opt[A] = A | OptGroup[A]

  def setting(v: Boolean | Seq[Opt[Int]]) = ()
  def v: Seq[Opt[Int]] = ???
  setting(v)
}
