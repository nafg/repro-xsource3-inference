object Hang {
  // Minimized code from scalajs-react
  val recursiveTypeAliases: RecursiveTypeAliases = RecursiveTypeAliases
  type ChildrenArray[A] = A | recursiveTypeAliases.ChildrenArray[A]

  sealed trait RecursiveTypeAliases {
    type ChildrenArray[A]
  }

  object RecursiveTypeAliases extends RecursiveTypeAliases {
    override type ChildrenArray[A] = Seq[Hang.ChildrenArray[A]]
  }
  type Void = Unit
  type Empty = Void | Null | Boolean
  type JsNumber = Byte | Short | Int | Float | Double
  trait Element
  type Node = ChildrenArray[Empty | String | JsNumber | Element]
  trait VdomNode {
    def rawNode: Node
  }

  // Minimized code from Scalably Typed bindings for Ant Design
  trait RenderedCell[RecordType]
  object ColumnType {
    def apply[RecordType](): ColumnType[RecordType] = ???

    implicit class ColumnTypeMutableBuilder[Self <: ColumnType[_], RecordType](
        val x: Self with ColumnType[RecordType]
    ) extends AnyVal {
      def setRender(
          value: (Any, RecordType, Double) => Node | RenderedCell[RecordType]
      ): Self = ???

    }
  }
  trait ColumnType[RecordType]

  // Minimized application code
  def render: Double => VdomNode = ???
  ColumnType[Double]()
    .setRender((_, t, _) => render(t).rawNode
    // .asInstanceOf[Node | RenderedCell[Double]]
    )
}
