// Minimized code from scalajs
trait |[A, B]
object | {
  sealed trait Evidence[-A, +B]

  private object ReusableEvidence extends Evidence[scala.Any, scala.Any]

  abstract sealed class EvidenceLowestPrioImplicits { this: Evidence.type =>
    implicit def right[A, B1, B2](implicit
        ev: Evidence[A, B2]
    ): Evidence[A, B1 | B2] =
      ReusableEvidence.asInstanceOf[Evidence[A, B1 | B2]]

    implicit def covariant[F[+_], A, B](implicit
        ev: Evidence[A, B]
    ): Evidence[F[A], F[B]] =
      ReusableEvidence.asInstanceOf[Evidence[F[A], F[B]]]

    implicit def contravariant[F[-_], A, B](implicit
        ev: Evidence[B, A]
    ): Evidence[F[A], F[B]] =
      ReusableEvidence.asInstanceOf[Evidence[F[A], F[B]]]
  }

  abstract sealed class EvidenceLowPrioImplicits
      extends EvidenceLowestPrioImplicits {
    this: Evidence.type =>
    implicit def intDouble: Evidence[Int, Double] =
      ReusableEvidence.asInstanceOf[Evidence[Int, Double]]
    implicit def left[A, B1, B2](implicit
        ev: Evidence[A, B1]
    ): Evidence[A, B1 | B2] =
      ReusableEvidence.asInstanceOf[Evidence[A, B1 | B2]]
  }

  object Evidence extends EvidenceLowPrioImplicits {
    implicit def base[A]: Evidence[A, A] =
      ReusableEvidence.asInstanceOf[Evidence[A, A]]
    implicit def allSubtypes[A1, A2, B](implicit
        ev1: Evidence[A1, B],
        ev2: Evidence[A2, B]
    ): Evidence[A1 | A2, B] =
      ReusableEvidence.asInstanceOf[Evidence[A1 | A2, B]]
  }

  import scala.language.implicitConversions

  implicit def from[A, B1, B2](a: A)(implicit
      ev: Evidence[A, B1 | B2]
  ): B1 | B2 =
    a.asInstanceOf[B1 | B2]
}
